<?php
/**
 * ROM Utility to build a collection of Bally/Stern Pinball ROMs into a single ROM file to be bank switched
 * so that we can easily swap between games without the need to program EPROMs.
 * 
 * Configure the games you want to include in the combined ROM in the file game_data.php
 *
 * Usage: from a MacOS, Linux, Unix or Windows (with PHP installed) command prompt type:
 * php ROM_builder.php [-bally][-stern]
 *
 * Build Options:
 * The -bally argument will omit any games that are not marked for inclusion in a bally only build for a 4MB EPROM.
 * The -stern argument will omit any games that are not marked for inclusion in a stern only build for a 4MB EPROM.
 *
 * The script will output two files, the combined ROM and a csv file detailing the games that were included and the DIP switch settings.
 * If the ROM extenstion is .incomplete then check the output or the gane_data file for errors or not enough games.
 * For games with extended ROM space (i.e. Vector v10) it MUST be allocated to be an even game in the game_data array.
 * 
 * PCB Revsion 3.0.2
 * 
 * DIP Switches:
 * 1  - EPROM A19 Select
 * 2  - EPROM A18 Select
 * 3  - EPROM A17 Select
 * 4  - EPROM A16 Select
 * 5  - EPROM A15 Select
 * 6  - EPROM A14 Select
 * 7  - EPROM A13 Select (not used for extended address space games)
 * 8  - Standard Address Space (OFF for extended address space)
 * 9  - Extended Address Space (OFF for standard address space)
 * 10 - External Memory Enable
 * One of DIP 8 & 9 must be selected never both ON or both OFF
 * 
 * 
 * Dave Langley
 *
 * September 2023
 * https://www.robotron-2084.co.uk/
 * https://gitlab.com/2084/rom-builder
 */

$version = '3.0.2';
$filename_version = str_replace('.', '_', $version);

echo 'Starting Bally/Stern Combined ROM Build ' . $version  . '...' . PHP_EOL . PHP_EOL;

// Load game data
(@include_once ('game_data.php')) OR die('Error: File game_data.php not found!' . PHP_EOL . PHP_EOL);
if (!isset($game_data) || !(is_array($game_data))) die ('Error: File game_data.php does not contain valid game_data array!' . PHP_EOL . PHP_EOL);

// Set up some variables and a 8k fill file containing FF
$multigame_rom = $fill_file = $csv_file = '';
for ($i=0; $i < 8192 ; $i++) { $fill_file .= chr(255); }
$bally_build = $stern_build = false;
$rom_pairing = ['u2' => 'u1', 'u6' => 'u5'];
$game_count = 0;

// Check for options
if (isset($argv[1]) && $argv[1] == '-bally') $bally_build = true;
if (isset($argv[1]) && $argv[1] == '-stern') $stern_build = true;

// Loop over all games
foreach ($game_data as $key => $game) {
    if (!$game['bally'] && $bally_build) continue;
    if (!$game['stern'] && $stern_build) continue;

    $game_count++;
    $error = false;
    $game_2764 = '';
    $game_combined_rom = '';
    $extended_rom_space = false;
    $source_path = __DIR__ . '/source_roms/' . ($game['src'] ? $game['src'] . '/' : '');
    $target_filename = __DIR__ . '/combined_roms/' . preg_replace('/[^A-Za-z0-9()-]+/', '_', strtolower($game['name'])) . ($game['extended'] ? '.27128' : '.2764');

    // Check all listed ROMs exist and load them into memory
    foreach ($game['roms'] as $rom_location => $rom_name) {
        if ($rom_name && file_exists($source_path . $rom_name)) {
            $roms[$rom_location] = file_get_contents($source_path . $rom_name);
        } else {
            $roms[$rom_location] = false;
        }
    }

    // Spilt ROMs to we can assemble them in address space order
    if ($game['extended'] && strlen($roms['u2']) == 32768 && !$roms['u1'] && !$roms['u5'] && !$roms['u6']) {
        // Special case for extended ROMs which are a single 256k file at U2.
        // ROMs are joined in the order U1, U5, U2 & U6 so that we can use A13 on EPROM Pin-28 to switch in the new address space 3000-3fff and 7000-7fff
        $extended_rom = $roms['u2'];
        $roms['u1'] = substr($extended_rom, hexdec(1000), hexdec(1000));  // 1000-1FFF  
        $roms['u2'] = substr($extended_rom, hexdec(3000), hexdec(1000));  // 3000-3FFF
        $roms['u5'] = substr($extended_rom, hexdec(5000), hexdec(1000));  // 5000-5FFF
        $roms['u6'] = substr($extended_rom, hexdec(7000), hexdec(1000));  // 7000-7FFF
        $extended_rom_space = true;
    } else {
        // For all other ROMs split u2/u6 if required after checking parent is 4k and child is empty
        foreach ($rom_pairing as $parent => $child) {
            if (strlen($roms[$parent]) == 4096) {
                if (!$roms[$child]) {
                    $roms[$child]  = substr($roms[$parent], 0, 2048);     // U2L/U1 1000-17FF   U6L/U5 1800-1FFF
                    $roms[$parent] = substr($roms[$parent], -2048);       // U2H/U2 5000-57FF   U6L/U6 5800-5FFF
                } else {
                    $error = true;
                }
            }
        }
    }

    // Check to see if any ROMs are not 2k, zero bytes or 4k if the extended ROM space flag is set
    foreach ($roms as $rom_location => $rom_data) {
        if ( !(strlen($rom_data) == 2048 || strlen($rom_data) == 0 || ($extended_rom_space && strlen($rom_data) == 4096)) ) $error = true;
    }

    // If there were no errors preparing the ROMs combine them into an 8k 2764 or 16k 27128 EPROM file
    if (!$error) {
        $game_combined_rom .= strlen($roms['u1']) == 0 ? $roms['u2'] : $roms['u1'];
        $game_combined_rom .= strlen($roms['u5']) == 0 ? $roms['u6'] : $roms['u5'];
        $game_combined_rom .= $roms['u2'] . $roms['u6'];
    }

    // Check we have built an 8k 2764 or a 16k 27128 EPROM file if the extended ROM space flag is set, if not fill with ff
    if (strlen($game_combined_rom) == 8192 || ($extended_rom_space && strlen($game_combined_rom) == 16384)) {
        $message = '[' . str_pad($game_count, 3, '0', STR_PAD_LEFT) .'] SUCCESS: Parsed ROMs for ' . $game['name'];

        // If this is an extended ROM then increment the game count again and print a message as it takes up double the space
        if ($extended_rom_space) {
            $game_count++;
            $message .= PHP_EOL . '[' . str_pad($game_count, 3, '0', STR_PAD_LEFT) .'] NOTICE:  Extended ROM space for ' . $game['name'];
        }

    } else {
        $error = true;
        $game_combined_rom = $fill_file;
        // if ($extended_rom_space) $game_combined_rom .= $game_combined_rom;
        $message = '[' . str_pad($game_count, 3, '0', STR_PAD_LEFT) .'] ERROR: Parsing of ROMs for ' . $game['name'] . ' failed';
    }

    // Add this game to multigame ROM
    $multigame_rom .= $game_combined_rom;

    // Echo message
    echo $message . PHP_EOL;

    // Save game 2764/27128 ROM
    $result = file_put_contents($target_filename, $game_combined_rom);
    if (!$result) {
        echo '[' . str_pad($game_count, 3, '0', STR_PAD_LEFT)  .'] Failed to save file ' . $target_filename . PHP_EOL;
    }
}

// Set file extension
$extension = 'incomplete';
if (strlen($multigame_rom) == 524288) {
    $extension ='27C040';
} else if (strlen($multigame_rom) == 1048576) {
    $extension ='27C080';
}

// Set the build name
$build_name = 'bally_stern';
if ($bally_build) {
    $build_name ='bally';
} else if ($stern_build) {
    $build_name ='stern';
}

// Save multigame ROM
$filename = __DIR__ . "/multigame_roms/{$build_name}_multigame_rom_v{$filename_version}.{$extension}";
$result = file_put_contents($filename, $multigame_rom);
if ($result) {
    echo PHP_EOL . 'Saved multi-game file ' . $filename . PHP_EOL;
} else {
    echo PHP_EOL . 'Failed to save multi-game file ' . $filename . PHP_EOL;
}

// Create csv file
$game_index = $game_count = 0;
foreach (array_reverse($game_data) as $game) {
    if (!$game['bally'] && $bally_build) continue;
    if (!$game['stern'] && $stern_build) continue;

    // Increment the game game_count
    $game_count++;

    $csv_file .= $game_count . ',' . '"' . $game['name'] . '","\''
              .  (($game_index & 64) == 64 ? '1' : '0')
              .  (($game_index & 32) == 32 ? '1' : '0')
              .  (($game_index & 16) == 16 ? '1' : '0')
              .  (($game_index &  8) ==  8 ? '1' : '0')
              .  (($game_index &  4) ==  4 ? '1' : '0')
              .  (($game_index &  2) ==  2 ? '1' : '0')
              .  (($game_index &  1) ==  1 ? '1' : '0')
              .  ($game['extended'] ? '01' : '10') // A13 select on V3 PCB
              .  '1' 
              .  '",' . PHP_EOL;

    // Increment the game game_index
    $game_index++;

    // For an extended ROM increment the game_index twice
    if ($game['extended']) $game_index++;
}

// Save csv file
$filename = __DIR__ . "/multigame_roms/{$build_name}_multigame_rom_names_v{$filename_version}_{$extension}.csv";
$result = file_put_contents($filename, $csv_file);
if ($result) {
    echo 'Saved CSV file ' . $filename . PHP_EOL;
} else {
    echo 'Failed to save CSV file ' . $filename . PHP_EOL;
}

echo PHP_EOL . 'Done!' . PHP_EOL;
