<?php
/**
 * Datafile for ROM Utility to build a collection of Bally/Stern Pinball ROMs into a single ROM file.
 * 
 * ROMs are in reverse order as the default state for the DIP switches with pull ups is high.
 *
 * No ROMs are included in this repository though they should all be freely available online.
 * 
 * Each array element of $game_data represents a game to be combined and has the following elelents:
 * name      The game's name
 * roms[]    An array contining the files names of u1, u2, u5 and u6. If the game is question does not have a specific ROM then set the value to false
 * src       The subfolder of /source_roms/ that the ROM exists in. If placing ROMs in /source_roms/ set the value to false
 * extended  Set to true or false to determine if this game uses the extended address space (e.g. Vector v10).
 * bally     Set to true or false depending on of you wish the game to be included with the command line option -bally when running ROM builder.
 * stern     Set to true or false depending on of you wish the game to be included with the command line option -stern when running ROM builder.
 *
 * All Free play and custom ROMs were created using patches from Olivers excellent work here: http://www.pinball4you.ch/okaegi/pro_soft.html
 * 
 * Dave Langley
 *
 * September 2023
 * https://www.robotron-2084.co.uk/
 * https://gitlab.com/2084/rom-builder
 * 
 * PCB Revsion 3.0.2
 */

// If your ROMs are named differenly feel free to change the data
$game_data = [
    // Test ROMs (2)
    ['name' => 'SAM III Test Fixture', 'roms' => ['u1' => false,  'u2' => 'SAM_III_U2.2716',      'u5' => false,  'u6' => 'SAM_III_U6.2716'],     'extended' => false, 'src' => 'stern', 'bally' => true,  'stern' => true],
    ['name' => 'Leon Test ROM v4',     'roms' => ['u1' => false,  'u2' => 'leon_test_rom_v4.U6',  'u5' => false,  'u6' => 'leon_test_rom_v4.U6'], 'extended' => false, 'src' => 'leon',  'bally' => true,  'stern' => true],

    // Stern M-200 MPU Games (21)
    ['name' => 'Orbitor 1',           'roms' => ['u1' => 'Orbit_u1.716', 'u2' => 'Orbit_u2.716', 'u5' => 'Orbit_u5.716', 'u6' => 'Orbit_u6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Iron Maiden',         'roms' => ['u1' => 'IronM_U1.716', 'u2' => 'IronM_U2.716', 'u5' => 'IronM_U5.716', 'u6' => 'IronM_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Lazer Lord',          'roms' => ['u1' => 'LLord_u1.716', 'u2' => 'LLord_u2.716', 'u5' => 'LLord_u5.716', 'u6' => 'LLord_u6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Dragonfist',          'roms' => ['u1' => 'DFist_U1.716', 'u2' => 'DFist_U2.716', 'u5' => 'DFist_U5.716', 'u6' => 'DFist_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Viper',               'roms' => ['u1' => 'Viper_U1.716', 'u2' => 'Viper_U2.716', 'u5' => 'Viper_U5.716', 'u6' => 'Viper_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Catacomb',            'roms' => ['u1' => 'Cata_U1.716',  'u2' => 'Cata_U2.716',  'u5' => 'Cata_U5.716',  'u6' => 'Cata_U6.716'],  'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Split Second',        'roms' => ['u1' => 'Split_U1.716', 'u2' => 'Split_U2.716', 'u5' => 'Split_U5.716', 'u6' => 'Split_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Lightning',           'roms' => ['u1' => 'Light_U1.716', 'u2' => 'Light_U2.716', 'u5' => 'Light_U5.716', 'u6' => 'Light_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Freefall',            'roms' => ['u1' => 'FreeF_U1.716', 'u2' => 'FreeF_U2.716', 'u5' => 'FreeF_U5.716', 'u6' => 'FreeF_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Nine Ball',           'roms' => ['u1' => '9Ball_U1.716', 'u2' => '9Ball_U2.716', 'u5' => '9Ball_U5.716', 'u6' => '9Ball_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Star Gazer',          'roms' => ['u1' => 'SGaz_U1.716',  'u2' => 'SGaz_U2.716',  'u5' => 'SGaz_U5.716',  'u6' => 'SGaz_U6.716'],  'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Flight 2000',         'roms' => ['u1' => 'F2000_U1.716', 'u2' => 'F2000_U2.716', 'u5' => 'F2000_U5.716', 'u6' => 'F2000_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Quicksilver',         'roms' => ['u1' => 'QSilv_U1P.716','u2' => 'QSilv_U2.716', 'u5' => 'QSilv_U5P.716','u6' => 'QSilv_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Cheetah',             'roms' => ['u1' => 'Cheet_U1.716', 'u2' => 'Cheet_U2.716', 'u5' => 'Cheet_U5.716', 'u6' => 'Cheet_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Seawitch',            'roms' => ['u1' => 'SWit_U1.716',  'u2' => 'SWit_U2.716',  'u5' => 'SWit_U5.716',  'u6' => 'SWit_U6.716'],  'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Big Game',            'roms' => ['u1' => 'BGame_U1.716', 'u2' => 'BGame_U2.716', 'u5' => 'BGame_U5.716', 'u6' => 'BGame_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Ali',                 'roms' => ['u1' => 'Ali_U1.716',   'u2' => 'Ali_U2.716',   'u5' => 'Ali_U5.716',   'u6' => 'Ali_U6.716'],   'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Galaxy',              'roms' => ['u1' => 'Galax_U1.716', 'u2' => 'Galax_U2.716', 'u5' => 'Galax_U5.716', 'u6' => 'Galax_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true],
    ['name' => 'Meteor (7-Digit)',    'roms' => ['u1' => 'Met7_U1.716',  'u2' => 'Met7_U2.716',  'u5' => 'Met7_U5.716',  'u6' => 'Met7_U6.716'],  'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Meteor (6-Digit/10)', 'roms' => ['u1' => 'Met10_U1.716', 'u2' => 'Met10_U2.716', 'u5' => 'Met10_U5.716', 'u6' => 'Met10_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Meteor',              'roms' => ['u1' => 'Met6_u1.716',  'u2' => 'Met6_u2.716',  'u5' => 'Met6_u5.716',  'u6' => 'Met6_u6.716'],  'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 


    // Stern M-100 MPU Games (11)
    ['name' => 'Magic',                                   'roms' => ['u1' => false, 'u2' => 'Magic_U2.716','u5' => false, 'u6' => 'Magic_U6.716'],'extended' => false, 'src' => 'stern', 'bally' => false,  'stern' => true],
    ['name' => 'Hot Hand',                                'roms' => ['u1' => false, 'u2' => 'HHand_U2.716','u5' => false, 'u6' => 'HHand_U6.716'],'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Trident',                                 'roms' => ['u1' => false, 'u2' => 'Tride_U2.716','u5' => false, 'u6' => 'Tride_U6.716'],'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Dracula',                                 'roms' => ['u1' => false, 'u2' => 'Drac_U2.716', 'u5' => false, 'u6' => 'Drac_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Nugent',                                  'roms' => ['u1' => false, 'u2' => 'Nug_U2.716',  'u5' => false, 'u6' => 'Nug_U6.716'],  'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Wild Fyre',                               'roms' => ['u1' => false, 'u2' => 'WFyre_U2.716','u5' => false, 'u6' => 'WFyre_U6.716'],'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Lectronamo',                              'roms' => ['u1' => false, 'u2' => 'Lectr_U2.716','u5' => false, 'u6' => 'Lectr_U6.716'],'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Memory Lane',                             'roms' => ['u1' => false, 'u2' => 'MLane_U2.716','u5' => false, 'u6' => 'MLane_U6.716'],'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Stars',                                   'roms' => ['u1' => false, 'u2' => 'Stars_U2.716','u5' => false, 'u6' => 'Stars_U6.716'],'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Stingray',                                'roms' => ['u1' => false, 'u2' => 'StRay_U2.716','u5' => false, 'u6' => 'StRay_U6.716'],'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 
    ['name' => 'Pinball',                                 'roms' => ['u1' => false, 'u2' => 'Pinb_U2.716', 'u5' => false, 'u6' => 'Pinb_U6.716'], 'extended' => false, 'src' => 'stern', 'bally' => false, 'stern' => true], 


    // Bally AS-2518-133 MPU Games (4)
    ['name' => 'Granny and the Gators',                   'roms' => ['u1' => false, 'u2' => 'G&TG2732.U2', 'u5' => false, 'u6' => 'G&TG2732.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Gold Ball (Oliver Custom v12)',           'roms' => ['u1' => false, 'u2' => 'gold_v12.u2', 'u5' => false, 'u6' => 'gold_v12.u6'], 'extended' => false, 'src' => 'okaegi','bally' => true,  'stern' => true],   
    ['name' => 'Gold Ball',                               'roms' => ['u1' => false, 'u2' => 'GOLD2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Baby Pac-Man',                            'roms' => ['u1' => false, 'u2' => '891-08_2.732','u5' => false, 'u6' => '891-15_6.732'],'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 


    // Bally AS-2518-35 MPU Games (80)
    ['name' => 'Cybernaut (Free Play)',                   'roms' => ['u1' => false, 'u2' => 'CYBE2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Cybernaut',                               'roms' => ['u1' => false, 'u2' => 'CYBE2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Fireball Classic (Free Play)',            'roms' => ['u1' => false, 'u2' => 'FB_CLASS.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Fireball Classic',                        'roms' => ['u1' => false, 'u2' => 'FB_CLASS.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Spy Hunter (Free Play)',                  'roms' => ['u1' => false, 'u2' => 'SPY_2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],     
    ['name' => 'Spy Hunter',                              'roms' => ['u1' => false, 'u2' => 'SPY_2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Black Pyramid (Free Play)',               'roms' => ['u1' => false, 'u2' => 'BLKP2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],  
    ['name' => 'Black Pyramid',                           'roms' => ['u1' => false, 'u2' => 'BLKP2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Kings of Steel (Free Play)',              'roms' => ['u1' => false, 'u2' => 'KNGS2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],  
    ['name' => 'Kings of Steel',                          'roms' => ['u1' => false, 'u2' => 'KNGS2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Xs and Os (Free Play)',                   'roms' => ['u1' => false, 'u2' => 'X&OS2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Xs and Os',                               'roms' => ['u1' => false, 'u2' => 'X&OS2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Grand Slam (4 Player)',                   'roms' => ['u1' => false, 'u2' => 'GNDSLAM4.U2', 'u5' => false, 'u6' => 'GRNDSLAM.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Grand Slam (2 Player)',                   'roms' => ['u1' => false, 'u2' => 'GRNDSLAM.U2', 'u5' => false, 'u6' => 'GRNDSLAM.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'BMX (Free Play)',                         'roms' => ['u1' => false, 'u2' => 'BMX_2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'BMX',                                     'roms' => ['u1' => false, 'u2' => 'BMX_2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Spectrum (Free Play)',                    'roms' => ['u1' => false, 'u2' => 'SPEC2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Spectrum',                                'roms' => ['u1' => false, 'u2' => 'SPEC2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Speakeasy (4 Player)',                    'roms' => ['u1' => false, 'u2' => '877-04_2.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Speakeasy (2 Player)',                    'roms' => ['u1' => false, 'u2' => 'EASY2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Mr and Mrs Pac-Man (Free Play)',          'roms' => ['u1' => false, 'u2' => 'MR&S2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],
    ['name' => 'Mr and Mrs Pac-Man',                      'roms' => ['u1' => false, 'u2' => 'MR&S2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Rapid Fire (Oliver Custom v3)',           'roms' => ['u1' => false, 'u2' => '869-04_2.732','u5' => false, 'u6' => 'rapidfuN.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Rapid Fire',                              'roms' => ['u1' => false, 'u2' => '869-04_2.732','u5' => false, 'u6' => '869-03_6.732'],'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Vector (Oliver Custom v10)',              'roms' => ['u1' => false, 'u2' => 'vector10.256','u5' => false, 'u6' => false],         'extended' => true,  'src' => 'okaegi','bally' => true,  'stern' => false],
    ['name' => 'Vector (Oliver Custom v5)',               'roms' => ['u1' => false, 'u2' => 'VECT_v5.U2',  'u5' => false, 'u6' => 'VECT_v5.U6'],  'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Vector',                                  'roms' => ['u1' => false, 'u2' => 'VECT2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Elektra (Free Play)',                     'roms' => ['u1' => false, 'u2' => 'ELEK2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Elektra',                                 'roms' => ['u1' => false, 'u2' => 'ELEK2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Centaur (Oliver Custom v27)',             'roms' => ['u1' => false, 'u2' => 'cent2uN.U2',  'u5' => false, 'u6' => 'cent2uN.U6'],  'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Centaur',                                 'roms' => ['u1' => false, 'u2' => 'CENT2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Medusa (Free Play)',                      'roms' => ['u1' => false, 'u2' => 'MDUS2732.U2', 'u5' => false, 'u6' => '720F5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Medusa',                                  'roms' => ['u1' => false, 'u2' => 'MDUS2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Fathom (Oliver Custom v5)',               'roms' => ['u1' => false, 'u2' => 'FATH_v5.U2',  'u5' => false, 'u6' => 'FATH_v5.U6'],  'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Fathom',                                  'roms' => ['u1' => false, 'u2' => 'FATH2732.U2', 'u5' => false, 'u6' => '720-5332.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Fireball II (Free Play)',                 'roms' => ['u1' => false, 'u2' => 'FB2_2732.U2', 'u5' => false, 'u6' => '720F5232.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Fireball II',                             'roms' => ['u1' => false, 'u2' => 'FB2_2732.U2', 'u5' => false, 'u6' => '720-5232.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Embryon (Oliver Custom v9)',              'roms' => ['u1' => false, 'u2' => 'EMBY_v9.U2',  'u5' => false, 'u6' => 'EMBY_v9.U6'],  'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Embryon',                                 'roms' => ['u1' => false, 'u2' => 'EMBY2732.U2', 'u5' => false, 'u6' => '720-5232.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Eight Ball Deluxe (Oliver Custom v32)',   'roms' => ['u1' => false, 'u2' => '8bdhuN.U2',   'u5' => false, 'u6' => '8bdhuN.U6'],   'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Eight Ball Deluxe',                       'roms' => ['u1' => false, 'u2' => '8DLX2732.U2', 'u5' => false, 'u6' => '720-5232.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Flash Gordon (Free Play)',                'roms' => ['u1' => false, 'u2' => 'FLSH2732.U2', 'u5' => false, 'u6' => '720F5232.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],
    ['name' => 'Flash Gordon',                            'roms' => ['u1' => false, 'u2' => 'FLSH2732.U2', 'u5' => false, 'u6' => '720-5232.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Xenon (Free Play)',                       'roms' => ['u1' => false, 'u2' => 'XENO2732.U2', 'u5' => false, 'u6' => '720F4032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],
    ['name' => 'Xenon',                                   'roms' => ['u1' => false, 'u2' => 'XENO2732.U2', 'u5' => false, 'u6' => '720-4032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Frontier (Free Play)',                    'roms' => ['u1' => false, 'u2' => 'FRNT2732.U2', 'u5' => false, 'u6' => '720F4032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],
    ['name' => 'Frontier',                                'roms' => ['u1' => false, 'u2' => 'FRNT2732.U2', 'u5' => false, 'u6' => '720-4032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Skateball (Oliver Custom v2)',            'roms' => ['u1' => false, 'u2' => 'skatefuN.U2', 'u5' => false, 'u6' => 'skatefuN.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Skateball',                               'roms' => ['u1' => false, 'u2' => 'SKAT2732.U2', 'u5' => false, 'u6' => '720-4032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Viking (Free Play/7-Digit)',              'roms' => ['u1' => false, 'u2' => 'VIKG2732.U2', 'u5' => false, 'u6' => '720F3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],
    ['name' => 'Viking',                                  'roms' => ['u1' => false, 'u2' => 'VIKG2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Mystic (Free Play/7-Digit)',              'roms' => ['u1' => false, 'u2' => 'MYST2732.U2', 'u5' => false, 'u6' => '720F3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false],
    ['name' => 'Mystic',                                  'roms' => ['u1' => false, 'u2' => 'MYST2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Hotdoggin (Oliver Custom v2)',            'roms' => ['u1' => false, 'u2' => 'hot2uN.U2',   'u5' => false, 'u6' => 'hot2uN.U6'],   'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Hotdoggin',                               'roms' => ['u1' => false, 'u2' => 'HOTD2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Rolling Stones (Free Play/7-Digit)',      'roms' => ['u1' => false, 'u2' => 'ROLL2732.U2', 'u5' => false, 'u6' => '720F3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Rolling Stones',                          'roms' => ['u1' => false, 'u2' => 'ROLL2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Space Invaders (Free Play/7-Digit)',      'roms' => ['u1' => false, 'u2' => 'INVA2732.U2', 'u5' => false, 'u6' => '720F3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false], 
    ['name' => 'Space Invaders',                          'roms' => ['u1' => false, 'u2' => 'INVA2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Silverball Mania (Free Play/7-Digit)',    'roms' => ['u1' => false, 'u2' => 'SILV2732.U2', 'u5' => false, 'u6' => '720F3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Silverball Mania',                        'roms' => ['u1' => false, 'u2' => 'SILV2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Nitro Ground Shaker (Free Play/7-Digit)', 'roms' => ['u1' => false, 'u2' => 'NITR2732.U2', 'u5' => false, 'u6' => '720F3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Nitro Ground Shaker',                     'roms' => ['u1' => false, 'u2' => 'NITR2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Dolly Parton (Free Play/7-Digit)',        'roms' => ['u1' => false, 'u2' => 'DOLL2732.U2', 'u5' => false, 'u6' => '720F3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Dolly Parton',                            'roms' => ['u1' => false, 'u2' => 'DOLL2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Supersonic',                              'roms' => ['u1' => false, 'u2' => 'SURP2732.U2', 'u5' => false, 'u6' => '720-3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Harlem Globetrotters On Tour',            'roms' => ['u1' => false, 'u2' => 'HARL2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Paragon (Free Play)',                     'roms' => ['u1' => false, 'u2' => 'PARA2732.U2', 'u5' => false, 'u6' => '720F3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Paragon',                                 'roms' => ['u1' => false, 'u2' => 'PARA2732.U2', 'u5' => false, 'u6' => '720-3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Kiss (Free Play)',                        'roms' => ['u1' => false, 'u2' => 'KISS2732.U2', 'u5' => false, 'u6' => '720F3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Kiss',                                    'roms' => ['u1' => false, 'u2' => 'KISS2732.U2', 'u5' => false, 'u6' => '720-3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Star Trek (Free Play)',                   'roms' => ['u1' => false, 'u2' => 'STAR2732.U2', 'u5' => false, 'u6' => '720F3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Star Trek',                               'roms' => ['u1' => false, 'u2' => 'STAR2732.U2', 'u5' => false, 'u6' => '720-3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Future Spa (Free Play/7-Digit)',          'roms' => ['u1' => false, 'u2' => 'FSPA2732.U2', 'u5' => false, 'u6' => '720F3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Future Spa',                              'roms' => ['u1' => false, 'u2' => 'FSPA2732.U2', 'u5' => false, 'u6' => '720-3532.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Voltan Escapes Cosmic Doom',              'roms' => ['u1' => false, 'u2' => 'VOLT2732.U2', 'u5' => false, 'u6' => '720-3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Playboy',                                 'roms' => ['u1' => false, 'u2' => 'PLAY2732.U2', 'u5' => false, 'u6' => '720-3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Six Million Dollar Man',                  'roms' => ['u1' => false, 'u2' => '6MIL2732.U2', 'u5' => false, 'u6' => '720-3032.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Lost World',                              'roms' => ['u1' => false, 'u2' => 'LOST2732.U2', 'u5' => false, 'u6' => '720-2832.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],


    // Bally AS-2518-17 MPU Games (10)
    ['name' => 'Strikes and Spares',                      'roms' => ['u1' => false, 'u2' => 'STSP2732.U2', 'u5' => false, 'u6' => '720-2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Mata Hari',                               'roms' => ['u1' => false, 'u2' => 'MATA2732.U2', 'u5' => false, 'u6' => '720-2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Power Play',                              'roms' => ['u1' => false, 'u2' => 'POWR2732.U2', 'u5' => false, 'u6' => '720-2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Eight Ball (Free Play)',                  'roms' => ['u1' => false, 'u2' => '8BAL2732.U2', 'u5' => false, 'u6' => '720F2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Eight Ball',                              'roms' => ['u1' => false, 'u2' => '8BAL2732.U2', 'u5' => false, 'u6' => '720-2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Evel Knievel',                            'roms' => ['u1' => false, 'u2' => 'EVEL2732.U2', 'u5' => false, 'u6' => '720-2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Black Jack (Free Play)',                  'roms' => ['u1' => false, 'u2' => 'BLKJ2732.U2', 'u5' => false, 'u6' => '720F2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => false, 'stern' => false], 
    ['name' => 'Black Jack',                              'roms' => ['u1' => false, 'u2' => 'BLKJ2732.U2', 'u5' => false, 'u6' => '720-2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => true],
    ['name' => 'Night Rider',                             'roms' => ['u1' => false, 'u2' => 'NGHT2732.U2', 'u5' => false, 'u6' => '720-2132.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
    ['name' => 'Freedom',                                 'roms' => ['u1' => false, 'u2' => 'FREE2732.U2', 'u5' => false, 'u6' => 'FREE2732.U6'], 'extended' => false, 'src' => 'bally', 'bally' => true,  'stern' => false],
];